import * as types from './../constants/ActionTypes';

//mac dinh
var initialState = false;

var myReducer = (state = initialState, action) => {
    //thuc thi hanh dong ung voi tung type action
    switch (action.type){
        case types.TOGGLE_FORM:
            return !state;
        case types.OPEN_FORM:
            return true;
        case types.CLOSE_FORM:
            return false;
        default:
            return state;
    }
}

export default myReducer;