import * as types from '../constants/ActionTypes';

//mac dinh
var initialState = {
    name : '',
    description : '',
    state : 1,
    id : '',
};

var myReducer = (state = initialState, action) => {
    //thuc thi hanh dong ung voi tung type action
    switch (action.type){
        case types.SELECTED_ITEM:
            return action.task;
        case types.CLEAR_EDITTING_ITEM:
            state = {
                name : '',
                description : '',
                state : 1,
                id : '',
            };
            return state;
        default:
            return state;
    }
}

export default myReducer;