import * as types from './../constants/ActionTypes';

var tasks = JSON.parse(localStorage.getItem('tasks'));
tasks = tasks?tasks:[];

var idMax = localStorage.getItem('idMax');
idMax = idMax?idMax:1;

var findIndex=(tasks, id)=>{
    let result = -1;
    tasks.forEach((task, key)=>{
        if(task.id === id) result = key;
    });
    return result;
}

var initialState = tasks;

var myReducer = (state = initialState, action) => {
    //thuc thi hanh dong ung voi tung type action
    switch (action.type){
        case types.LIST_ALL:
            return [...state];
        case types.SAVE_TASK:
            var task = {
                id : action.task.id,
                name : action.task.name,
                description : action.task.description,
                state : parseInt( action.task.state )
            }

            //check edit
            if(action.task.id){
                let key = findIndex(state, action.task.id);
                if( key !== -1 ){
                    state[ key ] = task;
                }
            }else{
                //add
                task.id = idMax;
                idMax++;
                state.push(task);
                localStorage.setItem('idMax', idMax);
            }
            localStorage.setItem('tasks', JSON.stringify(state));
            return [...state];
        case types.UPDATE_STATUS_TASK:
            let id = findIndex(state, action.id);
            if( id !== -1 ){
                //khong the sua state cua 1 task bang cach sua truc tiep , phai copy ra bien moi roi gan lai
                //copy task
                //c1
                // let cloneTask = {...state[ id ]};
                // cloneTask.state = parseInt( action.newStatus );
                // state[ id ] = cloneTask;

                //c2
                state[ id ] = {
                    ...state[ id ],
                    state : parseInt( action.newStatus )
                }
            }
            localStorage.setItem('tasks', JSON.stringify(state));
            return [...state];
        case types.DELETE_TASK:
            let idRemove = findIndex(state, action.id);
            if( idRemove !== -1 ){
                state.splice(idRemove, 1);
            }
            localStorage.setItem('tasks', JSON.stringify(state));
            return [...state];
        default:
            return [...state];
    }
}

export default myReducer;