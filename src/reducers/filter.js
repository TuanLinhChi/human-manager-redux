import * as types from './../constants/ActionTypes';

//mac dinh
var initialState = {
    name : '',
    state : -1,
    sort : 0
};

var myReducer = (state = initialState, action) => {
    //thuc thi hanh dong ung voi tung type action
    switch (action.type){
        case types.FILTER_TABLE:
            return {
                name : action.filter.name,
                state : parseInt(action.filter.state),
                sort : action.filter.sort
            };
        default:
            return state;
    }
}

export default myReducer;