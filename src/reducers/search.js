import * as types from './../constants/ActionTypes';

//mac dinh
var initialState = '';

var myReducer = (state = initialState, action) => {
    //thuc thi hanh dong ung voi tung type action
    switch (action.type){
        case types.SEARCH:
            return action.keyWord;
        default:
            return state;
    }
}

export default myReducer;