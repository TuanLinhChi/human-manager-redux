import { combineReducers } from 'redux';
import task from './task';
import isDisplayForm from './isDisplayForm';
import itemEditing from './itemEditing';
import filter from './filter';
import search from './search';

//combine cac reduce
//khai bao cac reduce de combine
const myReducer = combineReducers({
    tasks : task,
    isDisplayForm,
    itemEditing,
    filter,
    search
});

export default myReducer;