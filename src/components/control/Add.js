import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from './../../actions/index';

class Add extends Component {
    onToggleForm = () => {
        var {edittingTask} = this.props;
        if(edittingTask && edittingTask.id){
            this.props.onOpenForm();
        }else{
            this.props.onToggleForm();
        }
        this.props.onClearEdittingItem();
    }

    render() {
        return (
            <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <button 
                    className="btn btn-success mr-2"
                    onClick={this.onToggleForm}
                >
                    <span className="fa fa-plus mr-2"></span>Add Task
                </button>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        edittingTask : state.itemEditing
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onToggleForm : () => {
            dispatch(actions.toggleForm());
        },
        onClearEdittingItem : () => {
            dispatch(actions.clearEdittingItem());
        },
        onOpenForm : () => {
            dispatch(actions.openForm());
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Add);
