import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from './../../actions/index';

class Filter extends Component {
    onChange=(event)=>{
        var value = event.target.value;
        this.props.onSearch(value);
    }

  render() {
    return (
        <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <div className="input-group">
                <input
                    name="keyWord"
                    type="text"
                    className="form-control"
                    placeholder="Enter key work ...."
                    onChange={ this.onChange }
                />
                <span className="input-group-btn">
                    <button className="btn btn-primary" type="button">
                        <span className="fa fa-search mr-3"></span>Search
                    </button>
                </span>
            </div>
        </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
    return {
        
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onSearch : (keyWord) => {
            dispatch(actions.search(keyWord));
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Filter)
