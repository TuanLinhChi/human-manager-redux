import React, { Component } from 'react';
import Filter from './control/Filter';
import Add from './control/Add';

class Control extends Component {
    render() {
        return (
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div className="row">
                    <Filter />
                    <Add />
                </div>
            </div>
        );
    }
}

export default Control;
