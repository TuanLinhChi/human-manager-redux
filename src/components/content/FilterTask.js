import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from './../../actions/index';

class FilterTask extends Component {
    constructor(props){
        super(props);
        this.state = {
            filterName : '',
            filterStatus : -1,
            sort : 0
        }
    }

    onChange=(event)=>{
        var name = event.target.name;
        var value = event.target.value;
        this.setState({
            [name] : value
        });
        //thực hiện hết function này thì mới thực hiện lưu state
        this.props.onFilter({
            name : name === 'filterName'?value:this.state.filterName,
            state : name === 'filterStatus'?value:this.state.filterStatus,
            sort : name === 'sort'?value:this.state.sort
        });
    }

    onSort=(event)=>{
        var value = event.target.value;
        this.props.onSort(value);
    }
    
  render() {
    return (
        <tr>
            <td></td>
            <td>
                <div className="row">
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <input
                            type="text"
                            className="form-control"
                            name="filterName"
                            onChange = { this.onChange }
                        />
                    </div>
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div className="dropdown">
                            <select name="sort" className="custom-select mr-sm-2" onChange={this.onChange}>
                                <option value={0}>Default</option>
                                <option value={1}>A->Z</option>
                                <option value={-1}>Z->A</option>
                            </select>
                        </div>
                    </div>
                </div>
            </td>
            <td>
                <select 
                    className="form-control" 
                    name="filterStatus"
                    onChange = { this.onChange }
                >
                    <option value={-1}>All</option>
                    <option value={1}>List</option>
                    <option value={2}>Working</option>
                    <option value={3}>Review</option>
                    <option value={4}>Completed</option>
                </select>
            </td>
            <td></td>
            <td></td>
        </tr>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
    return {
        
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onFilter : (filter)=>{
            dispatch(actions.filter(filter));
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FilterTask);
