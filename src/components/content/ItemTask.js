import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from './../../actions/index';

class ItemTask extends Component {
    constructor(props){
        super(props);
        this.state = {
            no : null,
            name : '',
            description : '',
            editName : false,
            editDescription : false
        }
    }

    componentWillMount(){
        var {task, no} = this.props;
        this.setState({
            no: no,
            name: task.name,
            description: task.description
        });
    }

    onChangeState=(event)=>{
        var {task} = this.props;
        var state = event.target.value;
        this.props.onUpdateStatusTask(task.id, state);
    }

    removeTask=()=>{
        var {task} = this.props;
        let check = window.confirm('Bạn có chắc chắn về hành động này?');
        if(check === true){
            this.props.onDeleteTask(task.id);
        }
        this.props.onCloseForm();
    }

    onShowEditting=()=>{
        var {task} = this.props;
        this.props.onSelectItem(task);
        this.props.onOpenForm();
    }

    onShowEdittingName=()=>{
        this.setState({
            editName : true
        });
    }

    onShowEdittingDescription=()=>{
        this.setState({
            editDescription : true
        });
    }

    onChangeTask=()=>{
        var {no, name, description} = this.state;
        this.props.onEditTask(no, name, description);
        this.setState({
            editName : false,
            editDescription : false
        });
    }

    onChange=(event)=>{
        var name = event.target.name;
        var value = event.target.value;
        value = name === 'state'?parseInt(value):value;
        this.setState({
            [name] : value
        });
    }

  render() {
    var {task} = this.props;
    var {no, editName, editDescription} = this.state;
    var newName = editName?'':task.name;
    var newDescription = editDescription?'':task.description;
    var editNameElement = editName?
                    <span>
                        <input type="text" name="name" value={task.name} onChange={this.onChange}></input>
                        <button type="button" className="border-0" onClick={this.onChangeTask}><span className="fa fa-save"></span></button>
                    </span>:'';
    var editDescriptionElement = editDescription?
                    <span>
                        <input type="text" name="description" value={task.description} onChange={this.onChange}></input>
                        <button type="button" className="border-0" onClick={this.onChangeTask}><span className="fa fa-save"></span></button>
                    </span>:'';
    return (
        <tr>
            <td>{ no }</td>
            <td>
                <span onClick={this.onShowEdittingName}>{ newName }</span>
                { editNameElement }
            </td>
            <td className="text-center">
                <select
                    className="form-control"
                    name="status"
                    value={task.state}
                    onChange={this.onChangeState}
                >
                    <option value={1}>List</option>
                    <option value={2}>Working</option>
                    <option value={3}>Review</option>
                    <option value={4}>Completed</option>
                </select>
            </td>
            <td>
                <span onClick={this.onShowEdittingDescription}>{ newDescription }</span>
                { editDescriptionElement }
            </td>
            <td className="text-center p-0 pt-2">
                <button
                    type="button"
                    className="btn btn-warning mr-1"
                    onClick={this.onShowEditting}
                >
                    <span className="fa fa-pencil"></span>
                </button>
                <button 
                    type="button"
                    className="btn btn-danger"
                    onClick={this.removeTask}
                >
                    <span className="fa fa-trash"></span>
                </button>
            </td>
        </tr>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
    return {}
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onUpdateStatusTask: (id, newStatus) => {
            dispatch(actions.updateStatusTask(id, newStatus))
        },
        onDeleteTask: (id) => {
            dispatch(actions.deleteTask(id))
        },
        onCloseForm : () => {
            dispatch(actions.closeForm());
        },
        onOpenForm : () => {
            dispatch(actions.openForm());
        },
        onSelectItem: (task) => {
            dispatch(actions.selectItem(task));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemTask);
