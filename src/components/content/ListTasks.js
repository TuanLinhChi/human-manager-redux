import React, { Component } from 'react';
import ItemTask from './ItemTask';
import FilterTask from './FilterTask';
import { connect } from 'react-redux';

class ListTasks extends Component {
    render() {
        var {tasks, isDisplayForm, filter, keyWord} = this.props;

        //search
        if(keyWord){
            tasks = tasks.filter((task)=>{
                return task.name.toLowerCase().indexOf(keyWord) !== -1
            });
        }

        //filter
        if(filter){
            if(filter.name){
                tasks = tasks.filter((task)=>{
                    return task.name.toLowerCase().indexOf(filter.name) !== -1
                });
            }

            tasks = tasks.filter((task)=>{
                if(filter.state === -1){
                    return task;
                }else{
                    return parseInt(task.state) === filter.state
                }
            });
        }

        //sort
        tasks = tasks.sort((task1, task2)=>{
            if(task1.name > task2.name){
                return filter.sort;
            }else if(task1.name < task2.name){
                return -filter.sort;
            }else{
                return 0
            }
        });

        var elements = tasks.map((task)=>{
            return <ItemTask 
                        key={ task.id } 
                        task={ task }
                        no={ task.id }
                    />
        });
        return (
            <div className={isDisplayForm===true?"col-xs-8 col-sm-8 col-md-8 col-lg-8":"col-xs-12 col-sm-12 col-md-12 col-lg-12"}>
                <table className="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th className="text-center">#</th>
                            <th className="text-center">Name</th>
                            <th className="text-center">State</th>
                            <th className="text-center">Description</th>
                            <th className="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <FilterTask 
                            onSort={ this.props.onSort }
                        />
                        { elements }
                    </tbody>
                </table>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        tasks: state.tasks,
        isDisplayForm : state.isDisplayForm,
        filter : state.filter,
        keyWord : state.search
    }
}

const mapDispatchToProps = null;

export default connect(mapStateToProps, mapDispatchToProps)(ListTasks);
