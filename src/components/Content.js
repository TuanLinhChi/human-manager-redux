import React, { Component } from 'react';
import AddTask from './content/AddTask';
import ListTasks from './content/ListTasks';
import { connect } from 'react-redux';

class Content extends Component {
  render() {
    var {isDisplayForm} = this.props;
    var elementAdd = isDisplayForm===true?
        <AddTask/>:null
    return (
        <div className="row mt-15">
            <ListTasks />
            {elementAdd}
        </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
    return {
        //state tren store
        isDisplayForm : state.isDisplayForm    
    };
};

const mapDispatchToProps = null;

export default connect(mapStateToProps, mapDispatchToProps)(Content);
