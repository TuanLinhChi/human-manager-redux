import React, { Component } from 'react';
import Control from './components/Control'
import Content from './components/Content'
import './App.css';

//import all
// import _ from 'lodash';
//import { findIndex } from 'lodash';

class App extends Component {
    render() {        
        return (
            <div className="container">
                <div className="row mt-15 text-center mb-5 mt-5 pl-5">
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-3 text-danger">
                        <h1>My Task Manager</h1>
                    </div>
                    <Control />
                </div>
                <Content />
            </div>
        );
    }
}

export default App;
