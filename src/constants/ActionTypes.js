//khai bao loai action cho tung action
export const LIST_ALL = 'LIST_ALL';
export const SAVE_TASK = 'SAVE_TASK';

//Form
export const TOGGLE_FORM = 'TOGGLE_FORM';
export const CLOSE_FORM = 'CLOSE_FORM';
export const OPEN_FORM = 'OPEN_FORM';

//Edit task
export const UPDATE_STATUS_TASK = 'UPFATE_STATUS_TASK';
export const DELETE_TASK = 'DELETE_TASK';
export const SELECTED_ITEM = 'SELECTED_ITEM';
export const CLEAR_EDITTING_ITEM = 'CLEAR_EDITTING_ITEM';

//filter
export const FILTER_TABLE = 'FILTER_TABLE';

//sort
export const SEARCH = 'SEARCH';