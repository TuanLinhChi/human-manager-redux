import * as types from './../constants/ActionTypes';

//tra ve tu khoa ung voi tung action
export const listAll = () => {
    return {
        type : types.LIST_ALL
    }
}

export const saveTask = (task) => {
    return {
        type : types.SAVE_TASK,
        task
    }
}

export const toggleForm = () => {
    return {
        type : types.TOGGLE_FORM
    }
}

export const openForm = () => {
    return {
        type : types.OPEN_FORM
    }
}

export const closeForm = () => {
    return {
        type : types.CLOSE_FORM
    }
}

export const updateStatusTask = (id, newStatus) => {
    return {
        type : types.UPDATE_STATUS_TASK,
        id,
        newStatus
    }
}

export const deleteTask = (id) => {
    return {
        type : types.DELETE_TASK,
        id
    }
}

export const selectItem = (task) => {
    return {
        type : types.SELECTED_ITEM,
        task
    }
}

export const clearEdittingItem = () => {
    return {
        type : types.CLEAR_EDITTING_ITEM
    }
}

export const filter = (filter) => {
    return {
        type : types.FILTER_TABLE,
        filter
    }
}

export const search = (keyWord) => {
    return {
        type : types.SEARCH,
        keyWord
    }
}